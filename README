# Book Finder

Book Finder is a simple tool for book prices matching. Supported data providers

  - Allegro
  - Ebay
  - Unfortunately not Amazon due too Amazon Associate account verification issue


### Installation

Book Finder requires python3.6+, redis, and nodejs to run.

Create virtualenv and install the dependencies.

```sh
$ virtualenv -p python3.7 venv
$ source /venv/bin/activate
$ pip install -r requirements.txt
```

Install node dependencies and build frontend

```sh
$ cd www
$ npm install
$ npm run build
```

Run redis
```sh
$ cd ops
$ docker-compose up
```

Export configuration environment variables
```sh
$ export ALLEGRO_CLIENT_ID=...
$ export ALLEGRO_SECRET=...
$ export EBAY_APP_ID=...
$ export EBAY_CERT_ID=...
```

Run task worker (with active virtualenv)
```sh
$ PYTHONPATH=. dramatiq server.worker
```

Run web application (with active venv and exported env variables)
```sh
$ PYTHONPATH=. python3.6 server/app.py
```

Application is listening on [http://localhost:9000](http://localhost:9000)
