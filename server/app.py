import os

from sanic import Sanic
from sanic.exceptions import ServerError
from sanic.response import json, redirect, text, file
from sanic_cors import CORS

from server.business_logic.cq.book_pricing_query import BookPricingQuery
from server.business_logic.cq.book_query import BookQuery
from server.business_logic.cq.get_offer_task_query import GetOfferTaskQuery
from server.business_logic.cq.schedule_offer_task_command import ScheduleOfferTaskCommand
from server.infrastructure.config import initialize_config
from server.infrastructure.factories import cq_factory
from server.infrastructure.errors import NotFoundError

package_directory = os.path.dirname(os.path.abspath(__file__))
config = initialize_config()

app = Sanic()
app.static('/static', os.path.join(package_directory, 'public'))
app.static('/index.js', os.path.join(package_directory, 'public', 'index.js'))

CORS(app)


@app.route("/", methods=['GET'])
async def index(request):
    return await file(os.path.join(package_directory, 'public', 'index.html'))


@app.route("/book", methods=['GET', 'OPTIONS'])
async def book(request):
    if request.method == 'OPTIONS':
        return json({})

    try:
        search = " ".join(request.args['title'])
    except KeyError:
        raise ServerError("Never trust anyone who has not brought a book with them.", status_code=400)

    book_query = cq_factory.get(BookQuery)
    result = await book_query.process(title=search)

    return json(result)


@app.route("/book/<isbn>/offer_sync", methods=['GET', 'OPTIONS'])
async def offer_sync(request, isbn):
    book_pricing_query = cq_factory.get(BookPricingQuery)

    try:
        result = await book_pricing_query.process(isbn=isbn)
    except NotFoundError:
        raise ServerError("Book not found", status_code=404)

    return json(result)


@app.route("/book/<isbn>/offer", methods=['GET', 'OPTIONS'])
async def offer(request, isbn):
    if request.method == 'OPTIONS':
        return json({})

    schedule_command = cq_factory.get(ScheduleOfferTaskCommand)
    resource_id = await schedule_command.process(isbn=isbn)

    return redirect(to='/queue/{}'.format(resource_id), status=202, headers={
        'Access-Control-Expose-Headers': 'Location'
    })


@app.route("/queue/<resource_id>", methods=['GET', 'OPTIONS'])
async def queue_result(request, resource_id):
    offer_task_query = cq_factory.get(GetOfferTaskQuery)
    result = await offer_task_query.process(resource_id)
    return json(result)


@app.route("/health")
async def health(request):
    return json({"status": "ok"})


if __name__ == "__main__":
    app.run(host=config.host, port=config.port)
