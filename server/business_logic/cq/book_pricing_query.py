class BookPricingQuery:
    def __init__(self, service__isbn, service__allegro, service__ebay):
        self._isbn_service = service__isbn

        self._integrations = {
            'Allegro': service__allegro,
            'Ebay': service__ebay
        }

    async def process(self, isbn):
        book_info = await self._isbn_service.get(isbn=isbn)

        result = {
            'book_info': book_info,
            'isbn_search': [],
            'title_search': []
        }

        for provider, integration in self._integrations.items():
            isbn_result = await integration.find(search=isbn)

            if isbn_result:
                result['isbn_search'].append({
                    'provider': provider,
                    'result': isbn_result
                })
            else:
                title_result = await integration.find(search=book_info['title'])

                if title_result:
                    result['title_search'].append({
                        'provider': provider,
                        'result': title_result
                    })

        return result
