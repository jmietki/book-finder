class BookQuery:
    def __init__(self, service__isbn):
        self._isbn_service = service__isbn

    async def process(self, title):
        if not title:
            return []

        result = await self._isbn_service.search(title)
        return result
