class GetOfferTaskQuery:
    def __init__(self, repo__task_result):
        self._task_result_repo = repo__task_result

    async def process(self, queue_key):
        result = await self._task_result_repo.get_result(queue_key)
        return result

