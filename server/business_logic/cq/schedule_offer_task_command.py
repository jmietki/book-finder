import shortuuid

from server.worker import find_offers


class ScheduleOfferTaskCommand:
    def __init__(self, repo__task_result):
        self._task_result_repo = repo__task_result

    async def process(self, isbn):
        queue_key = shortuuid.uuid()

        value = {
            'status': 'IN PROGRESS'
        }

        await self._task_result_repo.set_result(queue_key, value)
        find_offers.send(queue_key=queue_key, isbn=isbn)

        return queue_key
