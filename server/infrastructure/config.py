import os

from pyioc.locators import KeyAlreadyRegisteredError

from server.infrastructure.ioc import service_container, cq_container, repo_container


class AppConfig:
    def __init__(self, host, port, redis_url, allegro, ebay):
        self.host = host
        self.port = port
        self.redis_url = redis_url
        self.allegro = allegro
        self.ebay = ebay


class AllegroConfig:
    def __init__(self, client_id, secret):
        self.client_id = client_id
        self.secret = secret


class EbayConfig:
    def __init__(self, app_id, cert_id):
        self.app_id = app_id
        self.cert_id = cert_id


def get_config():
    allegro_config = AllegroConfig(
        client_id=os.environ["ALLEGRO_CLIENT_ID"],
        secret=os.environ["ALLEGRO_SECRET"]
    )

    ebay_config = EbayConfig(
        app_id=os.environ["EBAY_APP_ID"],
        cert_id=os.environ["EBAY_CERT_ID"]
    )

    app_config = AppConfig(
        host=os.environ.get("HOST", "0.0.0.0"),
        port=os.environ.get("PORT", 9000),
        redis_url=os.environ.get("REDIS_URL", "redis://localhost"),
        allegro=allegro_config,
        ebay=ebay_config,
    )

    return app_config


def initialize_config():
    config = get_config()

    try:
        service_container.register_object('config', config)
        cq_container.register_object('config', config)
        repo_container.register_object('config', config)
    except KeyAlreadyRegisteredError:
        pass

    return config
