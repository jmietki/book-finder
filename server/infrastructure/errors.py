class ServiceQueryError(Exception):
    def __init__(self, url, status, message, msg=None):
        if msg is None:
            msg = "Failed to query resource {}. Status: {}. Response: {}".format(url, status, message)

        super().__init__(msg)

        self.url = url
        self.status = status
        self.message = message


class NotFoundError(Exception):
    pass