from server.infrastructure.ioc import cq_container


class CQFactory(object):
    def __init__(self, container):
        self._container = container

    def get(self, cq_class, context=None):
        return self._container.build(cq_class, context=context)


cq_factory = CQFactory(container=cq_container)
