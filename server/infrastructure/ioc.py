from pyioc.containers import NamespacedContainer

from server.ports.allegro import AllegroPort
from server.ports.ebay import EbayPort
from server.ports.isbn import IsbnService
from server.ports.repo.task_result import TaskResultRepo

repo_container = NamespacedContainer('repo')
repo_container.register_callable_with_deps('task_result', TaskResultRepo)

service_container = NamespacedContainer('service')
service_container.add_sub_container(repo_container)
service_container.register_callable_with_deps('isbn', IsbnService)
service_container.register_callable_with_deps('allegro', AllegroPort)
service_container.register_callable_with_deps('ebay', EbayPort)

cq_container = NamespacedContainer('cq')
cq_container.add_sub_container(repo_container)
cq_container.add_sub_container(service_container)
