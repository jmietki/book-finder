import base64
from http import HTTPStatus

import aiohttp

from server.infrastructure.errors import ServiceQueryError


class AllegroPort:
    LISTINGS_ENDPOINT_URL = 'https://api.allegro.pl/offers/listing'
    OAUTH_ENDPOINT_URL = 'https://allegro.pl/auth/oauth/token?grant_type=client_credentials'

    def __init__(self, config):
        self._config = config
        self._token = None

    async def find(self, search):

        if not self._token:
            await self._authorize()

        params = {
            'phrase': search
        }

        try:
            resp_results = await self._call_service(params)
        except ServiceQueryError as ex:
            if ex.status == HTTPStatus.UNAUTHORIZED:
                await self._authorize()
                resp_results = await self._call_service(params)
            else:
                raise

        results = self._parse_results(resp_results['items']['promoted'])
        results += self._parse_results(resp_results['items']['regular'])

        try:
            result = min(results, key=lambda r: r['price'])
            result['self_url'] = 'https://allegro.pl/oferta/{}'.format(result.pop('id'))
        except ValueError:
            return None

        return result

    async def _authorize(self):
        auth = "{}:{}".format(self._config.allegro.client_id, self._config.allegro.secret).encode("utf-8")

        headers = {
            'Authorization': 'Basic {}'.format(base64.b64encode(auth).decode("utf-8"))
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(self.OAUTH_ENDPOINT_URL, headers=headers) as resp:
                result = await resp.json()
                self._token = result['access_token']

    async def _call_service(self, params):
        headers = {
            'Authorization': 'Bearer {}'.format(self._token),
            'Accept': 'application/vnd.allegro.public.v1+json',
            'Content-type': 'application/vnd.allegro.public.v1+json'
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(self.LISTINGS_ENDPOINT_URL, params=params, headers=headers) as resp:
                if resp.status == HTTPStatus.OK:
                    return await resp.json()

                result = await resp.text()
                raise ServiceQueryError(resp.url, resp.status, message=result)

    @staticmethod
    def _parse_results(results):
        return [{
            'price': r['sellingMode']['price']['amount'],
            'currency': r['sellingMode']['price']['currency'],
            'title': r['name'],
            'id': r['id']
        } for r in results]
