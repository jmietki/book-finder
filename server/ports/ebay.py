import base64
from http import HTTPStatus

import aiohttp

from server.infrastructure.errors import ServiceQueryError


class EbayPort:
    LISTINGS_ENDPOINT_URL = 'https://api.ebay.com/buy/browse/v1/item_summary/search'
    OAUTH_ENDPOINT_URL = 'https://api.ebay.com/identity/v1/oauth2/token'

    def __init__(self, config):
        self._config = config
        self._token = None

    async def find(self, search):

        if not self._token:
            await self._authorize()

        params = {
            'q': search,
            'limit': 10,
            'category_ids': 2228,
            'filter': 'conditions:{NEW}'
        }

        try:
            resp_results = await self._call_service(params)
        except ServiceQueryError as ex:
            if ex.status == HTTPStatus.UNAUTHORIZED:
                await self._authorize()
                resp_results = await self._call_service(params)
            else:
                raise

        try:
            results = self._parse_results(resp_results['itemSummaries'])
        except KeyError:
            results = []

        if not results:
            return None

        results = min(results, key=lambda r: r.get('price'))
        return results

    async def _authorize(self):
        auth = "{}:{}".format(self._config.ebay.app_id, self._config.ebay.cert_id).encode("utf-8")

        headers = {
            'Authorization': 'Basic {}'.format(base64.b64encode(auth).decode("utf-8"))
        }

        payload = {
            'grant_type': 'client_credentials',
            'scope': 'https://api.ebay.com/oauth/api_scope'
        }

        async with aiohttp.ClientSession() as session:
            async with session.post(self.OAUTH_ENDPOINT_URL, headers=headers, data=payload) as resp:
                result = await resp.json()
                self._token = result['access_token']

    async def _call_service(self, params):
        headers = {
            'Authorization': 'Bearer {}'.format(self._token)
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(self.LISTINGS_ENDPOINT_URL, params=params, headers=headers) as resp:
                if resp.status == HTTPStatus.OK:
                    return await resp.json()

                result = await resp.text()
                raise ServiceQueryError(resp.url, resp.status, message=result)

    @staticmethod
    def _parse_results(results):

        return [{
            'price': r['price']['value'],
            'currency': r['price']['currency'],
            'self_url': r['itemWebUrl'],
            'title': r['title']
        } for r in results]
