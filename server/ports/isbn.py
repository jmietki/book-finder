from http import HTTPStatus

import aiohttp

from server.infrastructure.errors import ServiceQueryError, NotFoundError


class IsbnService:
    GOOGLE_BOOKS_URL = 'https://www.googleapis.com/books/v1/volumes'

    async def search(self, query):
        if not query:
            return []

        params = {
            'q': query
        }

        resp_results = await self._call_service(params=params)

        try:
            items = resp_results['items']
        except KeyError:
            return []

        results = []
        for result in items:
            volume_info = result['volumeInfo']

            if 'industryIdentifiers' not in volume_info:
                continue

            try:
                author = ",".join(volume_info['authors'])
            except KeyError:
                author = None

            try:
                image_url = volume_info['imageLinks']['thumbnail']
            except KeyError:
                image_url = None

            results.append({
                'title': volume_info['title'],
                'author': author,
                'imageUrl': image_url,
                'isbn': volume_info['industryIdentifiers']
            })

        return results

    async def get(self, isbn):
        isbn_query = 'isbn:{}'.format(isbn)
        results = await self.search(query=isbn_query)

        try:
            return next(iter(results))
        except StopIteration:
            raise NotFoundError()

    async def _call_service(self, params):
        async with aiohttp.ClientSession() as session:
            async with session.get(self.GOOGLE_BOOKS_URL, params=params) as resp:
                if resp.status == HTTPStatus.OK:
                    return await resp.json()

                result = await resp.text()
                raise ServiceQueryError(resp.url, resp.status, message=result)
