import json

import aioredis


class TaskResultRepo:
    KEY_FORMAT = 'QUEUE_{}'

    def __init__(self, config):
        self._config = config

    async def set_result(self, key, value):
        redis_key = self.KEY_FORMAT.format(key)

        conn = await aioredis.create_connection(self._config.redis_url)
        await conn.execute('set', redis_key, json.dumps(value))
        conn.close()
        await conn.wait_closed()

    async def get_result(self, key):
        redis_key = 'QUEUE_{}'.format(key)

        conn = await aioredis.create_connection(self._config.redis_url)
        value = await conn.execute('get', redis_key)
        conn.close()
        await conn.wait_closed()

        return json.loads(value)
