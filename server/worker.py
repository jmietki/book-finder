import asyncio
import json
import logging

import aioredis
import dramatiq

from dramatiq.brokers.redis import RedisBroker

from server.business_logic.cq.book_pricing_query import BookPricingQuery
from server.infrastructure.config import initialize_config
from server.infrastructure.factories import cq_factory
from server.infrastructure.errors import NotFoundError
from server.ports.repo.task_result import TaskResultRepo

config = initialize_config()

redis_broker = RedisBroker(url=config.redis_url)
dramatiq.set_broker(redis_broker)


async def save_result(queue_key, result):
    redis_key = 'QUEUE_{}'.format(queue_key)

    conn = await aioredis.create_connection(config.redis_url)
    await conn.execute('set', redis_key, json.dumps(result))
    conn.close()
    await conn.wait_closed()


@dramatiq.actor
def find_offers(queue_key, isbn):
    logging.info('Received task find_offers(queue_key{}, isbn={})'.format(queue_key, isbn))

    book_pricing_query = cq_factory.get(BookPricingQuery)
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    try:
        result = loop.run_until_complete(book_pricing_query.process(isbn=isbn))
    except NotFoundError:
        result = {
            'status': 'ERROR',
            'message': 'ISBN not found'
        }

    task_result_repo = TaskResultRepo(config=config)
    loop.run_until_complete(task_result_repo.set_result(queue_key, result))

    logging.info('Finished task find_offers(queue_key{}, isbn={})'.format(queue_key, isbn))
