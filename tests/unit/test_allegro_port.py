from unittest.mock import Mock

import pytest

from server.ports.allegro import AllegroPort

TEST_DATA = {'items': {'promoted': [], 'regular': [
    {'id': '7771292048', 'name': 'Python 3 Object-oriented Programming - Second Edit',
     'seller': {'id': '36771816', 'company': True, 'superSeller': True},
     'promotion': {'emphasized': False, 'bold': False, 'highlight': False},
     'delivery': {'availableForFree': False, 'lowestPrice': {'amount': '8.00', 'currency': 'PLN'}},
     'images': [{'url': 'https://7.allegroimg.com/original/11dac7/32b90056402cbb140bc9a71419f7'}],
     'sellingMode': {'format': 'BUY_NOW', 'price': {'amount': '272.00', 'currency': 'PLN'}, 'popularity': 0},
     'stock': {'unit': 'UNIT', 'available': 10}, 'category': {'id': '91485'}},
    {'id': '7771293681', 'name': 'Python 3 Object Oriented Programming Dusty Phillip',
     'seller': {'id': '36771816', 'company': True, 'superSeller': True},
     'promotion': {'emphasized': False, 'bold': False, 'highlight': False},
     'delivery': {'availableForFree': False, 'lowestPrice': {'amount': '8.00', 'currency': 'PLN'}},
     'images': [{'url': 'https://c.allegroimg.com/original/115276/96ab93354a80beadca642ce4ef5c'}],
     'sellingMode': {'format': 'BUY_NOW', 'price': {'amount': '263.00', 'currency': 'PLN'}, 'popularity': 0},
     'stock': {'unit': 'UNIT', 'available': 10}, 'category': {'id': '91485'}}]}}


class TestAllegroPort:
    @pytest.mark.asyncio
    async def test_if_parse_results_correctly(self):
        async def authorize_mock():
            return

        async def call_mock(params):
            return TEST_DATA

        ebay_port = AllegroPort(config=Mock())
        ebay_port._authorize = authorize_mock
        ebay_port._call_service = call_mock

        expected_result = {'price': '263.00', 'currency': 'PLN',
                           'title': 'Python 3 Object Oriented Programming Dusty Phillip',
                           'self_url': 'https://allegro.pl/oferta/7771293681'}
        result = await ebay_port.find(search='TEST SEARCH')
        assert result == expected_result
