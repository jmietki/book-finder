from unittest.mock import Mock

import pytest

from server.ports.ebay import EbayPort

TEST_DATA = {
    "href": "https://api.ebay.com/buy/browse/v1/item_summary/search?q=python+3+object&limit=50&filter=conditions%3A%7BNEW%7D&offset=0",
    "total": 26,
    "next": "https://api.ebay.com/buy/browse/v1/item_summary/search?q=python+3+object&limit=50&filter=conditions%3A%7BNEW%7D&offset=50",
    "limit": 2,
    "offset": 0,
    "itemSummaries": [
        {
            "itemId": "v1|122854913942|0",
            "title": "Python 3 Object-Oriented Programming - Second Edition by Dusty Phillips...",
            "image": {
                "imageUrl": "https://i.ebayimg.com/thumbs/images/g/MqUAAOSw0IJZyfiv/s-l225.jpg"
            },
            "price": {
                "value": "45.75",
                "currency": "USD"
            },
            "itemHref": "https://api.ebay.com/buy/browse/v1/item/v1|122854913942|0",
            "seller": {
                "username": "tbargainbooks84",
                "feedbackPercentage": "96.3",
                "feedbackScore": 5011
            },
            "condition": "Brand New",
            "conditionId": "1000",
            "shippingOptions": [
                {
                    "shippingCostType": "FIXED",
                    "shippingCost": {
                        "value": "3.90",
                        "currency": "USD"
                    }
                }
            ],
            "buyingOptions": [
                "FIXED_PRICE"
            ],
            "currentBidPrice": {
                "value": "45.75",
                "currency": "USD"
            },
            "epid": "222089963",
            "itemWebUrl": "https://www.ebay.com/itm/Python-3-Object-Oriented-Programming-Second-Edition-by-Dusty-Phillips/122854913942?hash=item1c9ab93796:g:MqUAAOSw0IJZyfiv",
            "itemLocation": {
                "postalCode": "29607",
                "country": "US"
            },
            "categories": [
                {
                    "categoryId": "268"
                },
                {
                    "categoryId": "267"
                }
            ],
            "adultOnly": False
        },
        {
            "itemId": "v1|382704726648|0",
            "title": "Python 3 Object-oriented Programming : Unleash the Power of Python 3 Objects,...",
            "image": {
                "imageUrl": "https://i.ebayimg.com/thumbs/images/g/VAsAAOSwZ4RcKqbN/s-l225.jpg"
            },
            "price": {
                "value": "48.98",
                "currency": "USD"
            },
            "itemHref": "https://api.ebay.com/buy/browse/v1/item/v1|382704726648|0",
            "seller": {
                "username": "greatbookprices1",
                "feedbackPercentage": "98.5",
                "feedbackScore": 39002
            },
            "marketingPrice": {
                "originalPrice": {
                    "value": "50.12",
                    "currency": "USD"
                },
                "discountPercentage": "2",
                "discountAmount": {
                    "value": "1.14",
                    "currency": "USD"
                }
            },
            "condition": "Brand New",
            "conditionId": "1000",
            "thumbnailImages": [
                {
                    "imageUrl": "https://i.ebayimg.com/00/s/NDkxWDQwMA==/z/VAsAAOSwZ4RcKqbN/$_0.JPG?set_id=8800005007"
                }
            ],
            "shippingOptions": [
                {
                    "shippingCostType": "FIXED",
                    "shippingCost": {
                        "value": "0.00",
                        "currency": "USD"
                    }
                }
            ],
            "buyingOptions": [
                "FIXED_PRICE"
            ],
            "currentBidPrice": {
                "value": "48.98",
                "currency": "USD"
            },
            "epid": "222089963",
            "itemWebUrl": "https://www.ebay.com/itm/Python-3-Object-oriented-Programming-Unleash-the-Power-of-Python-3-Objects/382704726648?hash=item591afab278:g:VAsAAOSwZ4RcKqbN",
            "itemLocation": {
                "postalCode": "60087",
                "country": "US"
            },
            "categories": [
                {
                    "categoryId": "2228"
                },
                {
                    "categoryId": "267"
                }
            ],
            "adultOnly": False
        }
    ]
}


class TestEbayPort:
    @pytest.mark.asyncio
    async def test_if_parse_results_correctly(self):
        async def authorize_mock():
            return

        async def call_mock(params):
            return TEST_DATA

        ebay_port = EbayPort(config=Mock())
        ebay_port._authorize = authorize_mock
        ebay_port._call_service = call_mock

        expected_result = {'price': '45.75',
                           'currency': 'USD',
                           'self_url': 'https://www.ebay.com/itm/Python-3-Object-Oriented-Programming-Second-Edition-by-Dusty-Phillips/122854913942?hash=item1c9ab93796:g:MqUAAOSw0IJZyfiv',
                           'title': 'Python 3 Object-Oriented Programming - Second Edition by Dusty Phillips...'}
        result = await ebay_port.find(search='TEST SEARCH')
        assert result == expected_result
