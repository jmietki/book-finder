import pytest

from server.ports.isbn import IsbnService
from server.infrastructure.errors import NotFoundError

TEST_RESPONSE = {
    "kind": "books#volumes",
    "totalItems": 662,
    "items": [
        {
            "kind": "books#volume",
            "id": "F71kywAACAAJ",
            "etag": "2dsGhFNq+Hg",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/F71kywAACAAJ",
            "volumeInfo": {
                "title": "Siewca Wiatru",
                "authors": [
                    "Maja Lidia Kossakowska"
                ],
                "publishedDate": "2007",
                "industryIdentifiers": [
                    {
                        "type": "ISBN_10",
                        "identifier": "8360505446"
                    },
                    {
                        "type": "ISBN_13",
                        "identifier": "9788360505441"
                    }
                ],
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 651,
                "printType": "BOOK",
                "averageRating": 5.0,
                "ratingsCount": 1,
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "preview-1.0.0",
                "imageLinks": {
                    "smallThumbnail": "http://books.google.com/books/content?id=F71kywAACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
                    "thumbnail": "http://books.google.com/books/content?id=F71kywAACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api"
                },
                "language": "en",
                "previewLink": "http://books.google.pl/books?id=F71kywAACAAJ&dq=siewca+wiatru&hl=&cd=1&source=gbs_api",
                "infoLink": "http://books.google.pl/books?id=F71kywAACAAJ&dq=siewca+wiatru&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Siewca_Wiatru.html?hl=&id=F71kywAACAAJ"
            },
            "saleInfo": {
                "country": "PL",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "PL",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=F71kywAACAAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            }
        },
        {
            "kind": "books#volume",
            "id": "Xt1GAAAACAAJ",
            "etag": "KSRLKJfevus",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/Xt1GAAAACAAJ",
            "volumeInfo": {
                "title": "Siewca wiatru",
                "authors": [
                    "Maja Lidia Kossakowska"
                ],
                "publishedDate": "2004",
                "industryIdentifiers": [
                    {
                        "type": "ISBN_10",
                        "identifier": "8389011379"
                    },
                    {
                        "type": "ISBN_13",
                        "identifier": "9788389011374"
                    }
                ],
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 570,
                "printType": "BOOK",
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "preview-1.0.0",
                "language": "pl",
                "previewLink": "http://books.google.pl/books?id=Xt1GAAAACAAJ&dq=siewca+wiatru&hl=&cd=2&source=gbs_api",
                "infoLink": "http://books.google.pl/books?id=Xt1GAAAACAAJ&dq=siewca+wiatru&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Siewca_wiatru.html?hl=&id=Xt1GAAAACAAJ"
            },
            "saleInfo": {
                "country": "PL",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "PL",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=Xt1GAAAACAAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            }
        }
    ]
}


class TestIsbnService:
    @pytest.mark.asyncio
    async def test_if_correctly_parses_google_response(self,):
        async def mock(params):
            return TEST_RESPONSE

        isbn_service = IsbnService()
        isbn_service._call_service = mock
        result = await isbn_service.search(query='Test query')

        expected = [{'title': 'Siewca Wiatru', 'author': 'Maja Lidia Kossakowska',
                     'imageUrl': 'http://books.google.com/books/content?id=F71kywAACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api',
                     'isbn': [{'type': 'ISBN_10', 'identifier': '8360505446'},
                              {'type': 'ISBN_13', 'identifier': '9788360505441'}]},
                    {'title': 'Siewca wiatru', 'author': 'Maja Lidia Kossakowska', 'imageUrl': None,
                     'isbn': [{'type': 'ISBN_10', 'identifier': '8389011379'},
                              {'type': 'ISBN_13', 'identifier': '9788389011374'}]}]
        assert result == expected

    @pytest.mark.asyncio
    async def test_if_get_raises_when_isbn_not_found(self):
        async def mock(params):
            return {}

        isbn_service = IsbnService()
        isbn_service._call_service = mock

        with pytest.raises(NotFoundError):
            await isbn_service.get(isbn='TEST ISBN')
