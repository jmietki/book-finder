// noinspection ES6UnusedImports
import {h} from "hyperapp";
import {debounce} from "debounce";

import "./app.css";

const BOOK_COVER_PLACEHOLDER = "https://store.lexisnexis.com.au/__data/media/catalog/thumb//placeholder.jpg";

const Loader = () => {
    return <div className="Loader">Searching for books...</div>;
};

const BookPreview = () => (state, actions) => {

    const rows = state.searchResult.map((book) => {
        return (
            <div className="BookTile">
                <img className="BookTitle-image" src={book.imageUrl || BOOK_COVER_PLACEHOLDER}/>
                <div className="BookTitle-desc">
                    <div className="BookTitle-desc-title">{book.title}</div>
                    <div>{book.author}</div>
                </div>
                <div className="BookTitle-controls">
                    <button className="myButton" onclick={() => actions.selectBook(book)}>View prices</button>
                </div>
            </div>
        )
    });

    return (
        <div className="BookPreview">
            {state.isLoading && <Loader/>}
            <div className="BookPreview-grid">
                {rows}
            </div>
        </div>
    );
};

const TitleOfferList = () => (state) => {
    const results = state.booksOffers.title_search.map((offer) => {
        return (
            <tr>
                <td style="width: 10%">{offer.provider}</td>
                <td>{offer.result.title}</td>
                <td>{offer.result.price} {offer.result.currency}</td>
                <td>
                    <button className="myButton" onclick={() => window.location.href=offer.result.self_url}>Go buy it !</button>
                </td>
            </tr>
        );
    });

    return (
        <div className="OfferResults">
            <div className="OfferResults-header">Related Books found by title search:</div>
            <table>{results}</table>
        </div>
    );
};

const OfferList = () => (state) => {
    const results = state.booksOffers.isbn_search.map((offer) => {
        return (
            <tr>
                <td style="width: 10%">{offer.provider}</td>
                <td>{offer.result.title}</td>
                <td>{offer.result.price} {offer.result.currency}</td>
                <td>
                    <button className="myButton" onclick={() => window.location.href=offer.result.self_url}>Go buy it !</button>
                </td>
            </tr>
        );
    });

    return (
        <div className="OfferResults">
            <table>{results}</table>
        </div>
    );
};

const PricesPreview = () => (state) => {
    let content;

    if (state.isLoading) {
        content = <div className="Loader">Searching for best offers...</div>;
    } else {
        content = (
            <div className="OfferPreview">
                <div className="BookTile">
                    <img className="BookTitle-image" src={state.selectedBook.imageUrl || BOOK_COVER_PLACEHOLDER}/>
                    <div className="BookTitle-desc">
                        <div className="BookTitle-desc-title">{state.selectedBook.title}</div>
                        <div>{state.selectedBook.author}</div>
                    </div>
                </div>
                {!state.booksOffers.isbn_search.length && <div className="OfferPreview-no-result">No books found by ISBN search.</div>}
                <OfferList/>
                {state.booksOffers.title_search.length ? <TitleOfferList/> : null}
            </div>
        );
    }

    return content;
};


export default (state, actions) => {

    const debounced = debounce((event) => actions.updateSearchTerm(event.target.value), 400);

    return (
        <div className="App">
            <div className="background"/>
            <div className="SearchWrapper">
                <input
                    placeholder="What are you looking for ?"
                    className="Search"
                    oninput={debounced}
                />
            </div>
            {state.selectedBook ? <PricesPreview/> : <BookPreview/>}
        </div>
    );
}