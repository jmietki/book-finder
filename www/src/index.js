import {app, h} from "hyperapp";
import view from "./components/app";


const state = {
    searchTerm: null,
    searchResult: [],
    isLoading: false,
    selectedBook: null,
    booksOffers: []
};

const actions = {
    setSearchResult: (value) => (state) => ({...state, searchResult: value, isLoading: false}),

    updateSearchTerm: (value) => (state, actions) => {
        if (!value.length) {
            return {...state, searchResult: [], searchTerm: value, isLoading: false, selectedBook: null}
        }

        const url = new URL("book", window.location);
        url.searchParams.append("title", value);

        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then(function (myJson) {
                actions.setSearchResult(myJson);
            });


        return {...state, searchResult: [], searchTerm: value, isLoading: true, selectedBook: null}
    },

    selectBook: (book) => (state, actions) => {
        const isbn = book.isbn[0].identifier;
        const url = new URL(`book/${isbn}/offer`, window.location);

        fetch(url).then((response) => {
            let location = response.headers.get('Location');

            actions.pullOffers(location)
        });

        return {...state, selectedBook: book, isLoading: true}
    },

    pullOffers: (location) => (state, actions) => {
        let url = new URL(location, window.location);
        fetch(url).then((response) => {
            return response.json()
        }).then((result) => {
            if (result.status === 'IN PROGRESS') {
                setTimeout(actions.pullOffers.bind(this, location), 1000);
            } else {
                actions.updateOffers(result);
            }
        })
    },

    updateOffers: (result) => (state) => {
        return {...state, booksOffers: result, isLoading: false}
    }
};

const container = document.body;

app(state, actions, view, container);