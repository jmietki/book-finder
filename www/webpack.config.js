const path = require('path');
const Html = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');


module.exports = {
    entry: path.resolve(__dirname, 'src', 'index.js'),
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "../server/public")
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader"
            },
            {
                test: /\.css$/,
                loader: ["style-loader", "css-loader?url=false"]
            }
        ]
    },
    plugins: [
        new Html({title: 'Book Finder'}),
        new CopyPlugin([
            {from: 'assets', to: 'assets'},
        ]),
    ],
    devServer: {
        contentBase: path.resolve(__dirname, "public")
    }
};